package com.egoshard.dialogstyleexample;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

public class CustomDialog extends DialogFragment {

    private boolean mCustom;

    public CustomDialog() {
    }

    @SuppressLint("ValidFragment")
    public CustomDialog(boolean custom) {
        mCustom = custom;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        super.onCreateDialog(savedInstanceState);
        AlertDialog.Builder builder;
        if (mCustom) {
            builder = new AlertDialog.Builder(getActivity(), R.style.CustomTheme_Dialog);
            builder.setMessage("Custom");
        } else {
            builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("Standard");
        }
        AlertDialog dialog = builder.create();
        dialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {


            }
        });

        dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "CANCEL", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {


            }
        });
        startActivity(new Intent(getActivity(), MainActivity.class).putExtra("", ""));

        return dialog;

    }
}