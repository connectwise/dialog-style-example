package com.egoshard.dialogstyleexample;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onStandardClick(View view) {
        CustomDialog dialog = new CustomDialog();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        dialog.show(ft, "1");

    }

    public void onCustomClick(View view) {
        CustomDialog dialog = new CustomDialog(true);
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        dialog.show(ft, "2");
    }

}
